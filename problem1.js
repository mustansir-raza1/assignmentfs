/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/
// 
const fs = require('fs').promises
function createJSONFile(dirPath) {

    fs.mkdir(dirPath, { recursive: true })
    .then(()=>{
        console.log("Directory Created!");
    })
    fs.writeFile(`firstFile.txt`,"hello")
    .then(()=>{
        console.log("Your file is created")
    })
    fs. writeFile('secondFile.txt',"second file")
    .then((data)=>{
        console.log("your second file is created")
    })
    fs.unlink("firstFile.txt")
    .then(()=>{
        console.log("your first file is deleted sucessfully")
    })
    fs.unlink("secondFile.txt")
    .then(()=>{
        console.log("your second file is deleted sucessfully")
    })
            
    
}
// createJSONFile("./Random_JSON_Files");
module.exports = createJSONFile;
