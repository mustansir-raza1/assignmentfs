const fs = require('fs').promises

function Problem2(filePath) {
    fs.readFile(filePath, "utf-8")
        .then(() => {
            console.log("Your File is ready to read")
        })
        .catch((error) => {
            console.log("error in 1")
        })

    // convert the content to uppercase
    fs.readFile(filePath, "utf-8")
        .then((data) => {
            let upperCaseContent = data.toUpperCase();
            console.log(upperCaseContent)
            fs.writeFile("upperCase.txt", upperCaseContent)
                .then(() => {
                    console.log("your file 1 data in uppercase:")
                })
            fs.writeFile("filenames.txt", "upperCase.txt")
                .then(() => {
                    console.log("your filenames is created")
                })
        })

    // read a file and convert it into a lowercase
    fs.readFile("upperCase.txt", 'utf-8')
        .then((data) => {
            let lowerConvert = data.toLowerCase().split('.');
            let lowerString = lowerConvert.map(lower => lower.trim()).join("\n");
            fs.writeFile("lowerCase.txt", lowerString)
            fs.appendFile('./filenames.txt', "\nlowerCase.txt")
        })

    fs.readFile("lowerCase.txt", "utf-8")
        .then((data) => {
            let sortContent = data.split("\n")
            sortContent = sortContent.sort().join('\n');
            fs.writeFile("sortfile.txt", sortContent,)
            fs.appendFile('./filenames.txt', "\nsortfile.txt")
                .then(() => {
                    console.log("complete 1")
                })
                .catch(()=>{
                    console.log("have error is sorting")
                })
        })

    fs.readFile('filenames.txt', 'utf-8')
        .then((data) => {
            let numberOfFiles = data.trim().split('\n');
            numberOfFiles.forEach((eachFile) => {
                fs.unlink(eachFile)
                    .then(() => {
                        console.log(`${eachFile} deleted successfully`);
                    })
                    .catch(()=>{
                        console.log("have error in deleting files")
                    })
            })
        })
}

module.exports = Problem2;

